package com.example.turbo_az.service;

import com.example.turbo_az.dto.CarDto;
import com.example.turbo_az.dto.CreateCarDto;
import com.example.turbo_az.entity.Car;
import com.example.turbo_az.exception.CarNotFoundException;
import com.example.turbo_az.repository.CarRepository;
import com.example.turbo_az.search.CarSpecialSearch;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarServiceIplTest {

    @Mock
    private CarRepository repository;

    @Mock
    private ModelMapper mapper;
    @InjectMocks
    private CarServiceIpl carService;

    @Test
    void createCar()  {
        ModelMapper mapper1 =new ModelMapper();
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Baku");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        Car car = mapper1.map(createCarDto,Car.class);
        car.setId(1L);
        CarDto carDto =mapper1.map(car,CarDto.class);
        when(mapper.map(createCarDto,Car.class)).thenReturn(mapper1.map(createCarDto,Car.class));
        when(repository.save(any(Car.class))).thenReturn(car);
        when(mapper.map(car,CarDto.class)).thenReturn(carDto);
        CarDto carDto2 =carService.createCar(createCarDto);
        assertEquals(carDto2,carDto);
        verify(repository).save(any(Car.class));
        verify(mapper, times(2)).map(any(),any());
    }

    @Test
    void getCarById(){
        ModelMapper mapper1 =new ModelMapper();
        Long id =1L;
        CarDto carDto =new CarDto();
        carDto.setId(id);
        carDto.setName("Mercedes");
        carDto.setModel("C-Class");
        carDto.setYear(2000);
        carDto.setPrice(300000D);
        carDto.setCity("Baku");
        carDto.setCredit(true);
        carDto.setBarter(false);
        Car car =mapper1.map(carDto,Car.class);
        when(repository.findById(id)).thenReturn(Optional.of(car));
        when(mapper.map(car,CarDto.class)).thenReturn(carDto);
        CarDto carDto2 =carService.getCarById(id);
        assertEquals(carDto2,carDto);
        verify(repository).findById(any(Long.class));
        verify(mapper).map(any(),any());
    }
    @Test
    void getCarByIdThrowCarNotFoundException(){
        Long id = 1L;
        when(repository.findById(id)).thenThrow(new CarNotFoundException());
        assertThatThrownBy(()->carService.getCarById(id))
                .isInstanceOf(CarNotFoundException.class);
    }

    @Test
    void updateCar(){
        Long id = 1L;
        ModelMapper mapper1 =new ModelMapper();
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Baku");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        CarDto carDto =mapper1.map(createCarDto,CarDto.class);
        carDto.setId(id);
        Car car =mapper1.map(carDto,Car.class);
        when(repository.existsById(id)).thenReturn(true);
        when(mapper.map(createCarDto,CarDto.class)).thenReturn(carDto);
        when(mapper.map(carDto,Car.class)).thenReturn(car);
        when(repository.save(car)).thenReturn(car);
        assertEquals(carDto,carService.updateCar(id,createCarDto));
        verify(repository).save(any(Car.class));
        verify(repository).existsById(any(Long.class));
        verify((mapper),times(2)).map(any(),any());

    }

    @Test
    void updateCarByIdThrowCarNotFoundException(){
        Long id =1L;
        when(repository.existsById(id)).thenReturn(false);
        assertThatThrownBy(()->carService.updateCar(id,any(CreateCarDto.class)))
                .isInstanceOf(CarNotFoundException.class);

  }
    @Test
    void deleteCarById(){
        Long id =1L;
        String output="Deleting car id: "+id;
        when(repository.existsById(id)).thenReturn(true);
        assertEquals(carService.deleteCar(id),output);
        verify(repository).existsById(any(Long.class));
        verify(repository).deleteById(any(Long.class));
    }

    @Test
    void deleteCarByIdNotFoundException(){
        when(repository.existsById(any(Long.class))).thenReturn(false);
        assertThatThrownBy(()->carService.deleteCar(1L))
                .isInstanceOf(CarNotFoundException.class);
        verify(repository).existsById(any(Long.class));
    }

    @Test
    void getAllCars(){
        ModelMapper mapper1 =new ModelMapper();
        CarDto carDto1 =new CarDto();
        carDto1.setId(1L);
        carDto1.setName("Mercedes");
        carDto1.setModel("C-Class");
        carDto1.setYear(2000);
        carDto1.setPrice(300000D);
        carDto1.setCity("Baku");
        carDto1.setCredit(true);
        carDto1.setBarter(false);
        List<CarDto> carDtoList =new ArrayList<>();
        carDtoList.add(carDto1);
        List<Car> carList =mapper1.map(carDtoList,new TypeToken<List<Car>>(){}.getType());
        when(repository.findAll()).thenReturn(carList);
        when(mapper.map(carList,new TypeToken<List<CarDto>>(){}.getType())).thenReturn(carDtoList);
        List<CarDto> carDtoList2=carService.getAllCars();
        assertEquals(carDtoList,carDtoList2);
        verify(repository).findAll();
        verify(mapper).map(repository.findAll(), new TypeToken<List<CarDto>>(){}.getType());


    }

    @Test
    void searchSpecialCar(){

        CarSpecialSearch searchCar=new CarSpecialSearch();
        searchCar.setName("Mercedes");
        searchCar.setModel("C-Class");
        searchCar.setCity("Baku");
        searchCar.setMinPrice(0D);
        searchCar.setMaxPrice(1000000D);
        searchCar.setMinYear(2000);
        searchCar.setMaxYear(2021);
        searchCar.setBarter(false);
        searchCar.setCredit(true);
       Specification<Car> carSpecification = (root, query, cb) ->
        {
            List<Predicate> predicates = new ArrayList<>();
            if (searchCar.getName() != null &&
                    searchCar.getName().isEmpty() &&
                    searchCar.getName().isBlank()) {
                predicates.add(cb.equal(root.get("name"), searchCar.getName()));
            }
            if (searchCar.getModel() != null &&
                    searchCar.getModel().isEmpty() &&
                    searchCar.getModel().isBlank()) {
                predicates.add(cb.equal(root.get("model"), searchCar.getModel()));
            }
            if (searchCar.getCity() != null &&
                    searchCar.getCity().isEmpty() &&
                    searchCar.getCity().isBlank()) {
                predicates.add(cb.equal(root.get("city"), searchCar.getCity()));
            }
            if (searchCar.getMinPrice() != null) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("price"), searchCar.getMinPrice()));
            }
            if (searchCar.getMaxPrice() != null) {
                predicates.add(cb.lessThanOrEqualTo(root.get("price"), searchCar.getMaxPrice()));
            }
            if (searchCar.getMinYear() != null) {
                predicates.add(cb.greaterThanOrEqualTo(root.get("year"), searchCar.getMinYear()));
            }
            if (searchCar.getMaxYear() != null) {
                predicates.add(cb.lessThanOrEqualTo(root.get("year"), searchCar.getMaxYear()));
            }
            if (searchCar.getCredit() != null) {
                predicates.add(cb.equal(root.get("credit"), searchCar.getCredit()));
            }
            if (searchCar.getBarter() != null) {
                predicates.add(cb.equal(root.get("barter"), searchCar.getBarter()));
            }
            return cb.and(predicates.toArray(new Predicate[0]));
        };



        ModelMapper mapper1=new ModelMapper();
        List<Car> carList = new ArrayList<>();
        Car car = new Car();
        car.setId(1L);
        car.setName("Mercedes");
        car.setModel("C-Class");
        car.setCity("Baku");
        car.setYear(2012);
        car.setPrice(300000D);
        car.setCredit(true);
        car.setBarter(false);
        carList.add(car);
        List<CarDto> carDtoList =mapper1.map(carList,new TypeToken<List<CarDto>>(){}.getType());
        when(repository.findAll(carSpecification)).thenReturn(carList);
        when(mapper.map(carList,new TypeToken<List<CarDto>>(){}.getType())).thenReturn(carDtoList);
        List<CarDto> carDtoList2=carService.searchCar(searchCar);
        assertEquals(carDtoList,carDtoList2);
        verify(repository).findAll(any(Specification.class));
        verify(mapper).map(carList, new TypeToken<List<CarDto>>(){}.getType());
    }

}