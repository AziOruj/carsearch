package com.example.turbo_az.controller;

import com.example.turbo_az.dto.CarDto;
import com.example.turbo_az.dto.CreateCarDto;
import com.example.turbo_az.entity.Car;
import com.example.turbo_az.exception.CarNotFoundException;
import com.example.turbo_az.search.CarSpecialSearch;
import com.example.turbo_az.service.CarService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.engine.jdbc.spi.SqlExceptionHelper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureWebMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@AutoConfigureWebMvc
@ExtendWith(SpringExtension.class)
@WebMvcTest(CarController.class)
class CarControllerTest {
    private static final String MAIN_URL = "/v1/cars";

    @MockBean
    private  CarService carService;

    @Autowired
    private MockMvc mockMvc;

    @Test
    void createCar() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Baku");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        mockMvc.perform(post(MAIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated());
    }

    @Test
    void createCarBadRequest() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        mockMvc.perform(post(MAIN_URL)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto))
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getCarByIdSuccess() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        Long id =1L;
        CarDto carDto =new CarDto();
        carDto.setId(id);
        carDto.setName("Mercedes");
        carDto.setModel("C-Class");
        carDto.setYear(2000);
        carDto.setPrice(300000D);
        carDto.setCity("Baku");
        carDto.setCredit(true);
        carDto.setBarter(false);
        when(carService.getCarById(id)).thenReturn(carDto);
        mockMvc.perform(get(MAIN_URL+"/"+id))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(containsString(objectMapper.writeValueAsString(carDto))));

    }

    @Test
    void getCarByIdBadRequest() throws Exception{
        String id ="vs";
        mockMvc.perform(get(MAIN_URL+"/"+id))
                .andExpect(status().isBadRequest());
    }
    @Test
    void getCarByIdNotFound() throws Exception{
        Long id =12L;
        when(carService.getCarById(id)).thenThrow(new CarNotFoundException());
        mockMvc.perform(get(MAIN_URL+"/"+id))
                .andExpect(status().isNotFound());
    }

    @Test
    void updateCarSuccess() throws Exception {
        Long id =1L;
        ModelMapper mapper1=new ModelMapper();
        ObjectMapper objectMapper = new ObjectMapper();
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Baku");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        CarDto carDto =mapper1.map(createCarDto,CarDto.class);
        carDto.setId(id);
        when(carService.updateCar(id,createCarDto)).thenReturn(carDto);
        mockMvc.perform(put(MAIN_URL+"/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(carDto)));

    }

    @Test
    void updateCarBadRequest() throws Exception{
        ObjectMapper objectMapper =new ObjectMapper();
        String id_="vs";
        Long id =100L;
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Seki");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);

        mockMvc.perform(put(MAIN_URL+"/"+id_)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto)))
                .andExpect(status().isBadRequest());
        createCarDto.setName(null);
        when(carService.updateCar(id,createCarDto)).thenThrow(new CarNotFoundException());
        mockMvc.perform(put(MAIN_URL+"/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto)))
                .andExpect(status().isBadRequest());

    }

    @Test
    void updateCarNotFound() throws Exception{
        ObjectMapper objectMapper =new ObjectMapper();
        Long id =100L;
        CreateCarDto createCarDto=new CreateCarDto();
        createCarDto.setName("Mercedes");
        createCarDto.setModel("C-Class");
        createCarDto.setYear(2000);
        createCarDto.setPrice(300000D);
        createCarDto.setCity("Seki");
        createCarDto.setCredit(true);
        createCarDto.setBarter(false);
        when(carService.updateCar(id,createCarDto)).thenThrow(new CarNotFoundException());
        mockMvc.perform(put(MAIN_URL+"/"+id)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(createCarDto)))
                .andExpect(status().isNotFound());
    }

    @Test
    void deleteCarSuccess() throws Exception{
        Long id =1L;
        String output="Deleting car id: "+id;
        when(carService.deleteCar(id)).thenReturn(output);
        mockMvc.perform(delete(MAIN_URL+"/"+id))
                .andExpect(status().isOk())
                .andExpect(content().string(output));
    }

    @Test
    void deleteCarBadRequest() throws Exception{
        String id ="vs";
        mockMvc.perform(delete(MAIN_URL+"/"+id))
                .andExpect(status().isBadRequest());
    }

    @Test
    void deleteCarNotFound() throws Exception{
        Long id =1L;
        when(carService.deleteCar(id)).thenThrow(new CarNotFoundException());
        mockMvc.perform(delete(MAIN_URL+"/"+id))
                .andExpect(status().isNotFound());
    }

    @Test
    void getAllCars() throws  Exception{
        mockMvc.perform(get(MAIN_URL))
                .andExpect(status().isOk());
    }

    @Test
    void searchSpecialCar() throws Exception{
        ModelMapper mapper1=new ModelMapper();
        ObjectMapper objectMapper =new ObjectMapper();
        CarSpecialSearch search = new CarSpecialSearch();
        search.setModel("Mercedes");
        search.setModel("C-Class");
        List<Car> carList = new ArrayList<>();
        Car car = new Car();
        car.setId(1L);
        car.setName("Mercedes");
        car.setModel("C-Class");
        car.setCity("Baku");
        car.setYear(2012);
        car.setPrice(300000D);
        car.setCredit(true);
        car.setBarter(false);
        carList.add(car);
        List<CarDto> carDtoList =mapper1.map(carList,new TypeToken<List<CarDto>>(){}.getType());
        when(carService.searchCar(search)).thenReturn(carDtoList);
        mockMvc.perform(post(MAIN_URL+"/"+"search")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(search))
                .accept(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk())
                .andExpect(content().string(objectMapper.writeValueAsString(carDtoList)));

    }
}