package com.example.turbo_az.entity;


import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;

import javax.persistence.*;
import java.util.Set;
@Data
@Entity
@Table(name = "users")
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String login;
    private String password;
    @OneToMany(mappedBy = "userComments",fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Comments> commentsCars;
//    @ManyToMany(mappedBy = "carUser")
//    @JsonIgnore
//    private Set<Car> userCar;

}
