package com.example.turbo_az.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.Set;

@Entity
@Data
@NoArgsConstructor
@Table(name = "cars")
public class Car {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String model;
    private Integer year;
    private Double price;
    private String city;
    private Boolean credit;
    private Boolean barter;
    private Long countComments;
    @OneToMany(mappedBy = "carComments",fetch = FetchType.EAGER)
    @JsonIgnore
    private Set<Comments> commentsUser;
//    @ManyToMany
//    @JsonIgnore
//    @JoinTable(
//            name = "Cars_Users",
//            joinColumns = {
//                    @JoinColumn(name = "car_id",referencedColumnName="id")
//            },
//            inverseJoinColumns = {
//                    @JoinColumn(name = "user_id",referencedColumnName="id")
//            }
//    )
//    private Set<Users> carUser;




}
