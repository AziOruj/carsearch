package com.example.turbo_az.search;

import lombok.Data;

@Data
public class CarSpecialSearch {
    private String name;
    private String model;
    private String city;
    private Integer minYear;
    private Integer maxYear;
    private Double minPrice;
    private Double maxPrice;
    private Boolean credit;
    private Boolean barter;
}
