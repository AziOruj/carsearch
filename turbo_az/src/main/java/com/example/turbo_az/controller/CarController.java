package com.example.turbo_az.controller;
import com.example.turbo_az.dto.CarDto;
import com.example.turbo_az.dto.CreateCarDto;
import com.example.turbo_az.exception.CarNotFoundException;
import com.example.turbo_az.search.CarSpecialSearch;
import com.example.turbo_az.service.CarService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/v1/cars")
@RequiredArgsConstructor
@Slf4j
public class CarController {
    private final CarService carService;
    @PostMapping
    public ResponseEntity<CarDto> createCar(@Valid @RequestBody CreateCarDto car)  {
        log.info("Car create {}",car);
        return  ResponseEntity.status(HttpStatus.CREATED)
                .body(carService.createCar(car));
    }

    @GetMapping("/{id}")
    public ResponseEntity<CarDto> getCarById(@PathVariable Long id){
        log.info("Get Car by id {}",id);
        try { return ResponseEntity.ok(carService.getCarById(id));
        }catch (CarNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
    @PutMapping("/{id}")
    public ResponseEntity<CarDto> updateCar(@PathVariable Long id, @Valid @RequestBody CreateCarDto createCarDto){
        log.info("Update car by id {} {}",id, createCarDto);
        try {
            return ResponseEntity.ok(carService.updateCar(id,createCarDto));
        }catch (CarNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteCar(@PathVariable Long id){
        log.info("Deleted car by id {}",id);
        try{
            return ResponseEntity.ok(carService.deleteCar(id));
        }catch (CarNotFoundException e){
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
    }
    @GetMapping
    public ResponseEntity<List<CarDto>> getAllCars(){
         return ResponseEntity.ok(carService.getAllCars());
    }
    @PostMapping("/search")
    public  ResponseEntity<List<CarDto>> searchCar(@RequestBody CarSpecialSearch search){
        return ResponseEntity.ok(carService.searchCar(search));
    }
}

