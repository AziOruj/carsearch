package com.example.turbo_az.controller;

import com.example.turbo_az.dto.UserCreateDto;
import com.example.turbo_az.exception.UserIsExistsException;
import com.example.turbo_az.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/v1/signin")
@RequiredArgsConstructor
public class UserController {
    private final UserService userService;

    @PostMapping
    public ResponseEntity<String> createUser(@RequestBody @Valid UserCreateDto user) {
        try {

            return ResponseEntity.status(HttpStatus.CREATED).body(userService.createUser(user));
        } catch (UserIsExistsException ex) {

            return ResponseEntity.badRequest().body("Username " + user.getLogin() + " is already exists.");
        }


    }

}
