package com.example.turbo_az.controller;

import com.example.turbo_az.dto.CreditDto;
import com.example.turbo_az.dto.CreditDtoCreate;
import com.example.turbo_az.service.CreditService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/v1/credits")
@RequiredArgsConstructor
public class CreditController {
    private final CreditService creditService;
    @PostMapping()
    public ResponseEntity<CreditDto> createCredit(@RequestBody CreditDtoCreate create){


        return  ResponseEntity.status(HttpStatus.CREATED).body( creditService.createCredit(create));
    }
}
