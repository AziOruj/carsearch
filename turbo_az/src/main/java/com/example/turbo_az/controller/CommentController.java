package com.example.turbo_az.controller;

import com.example.turbo_az.dto.CommentCreateDto;
import com.example.turbo_az.dto.UserCreateDto;
import com.example.turbo_az.exception.UserIsExistsException;
import com.example.turbo_az.service.CommentService;
import com.example.turbo_az.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.net.URI;

@RestController
@RequestMapping("/v1/cars")
@RequiredArgsConstructor
public class CommentController {
    private final CommentService commentService;

    @PostMapping("/{id}")
    public void  createComment(@RequestBody CommentCreateDto comment, @PathVariable Long id){
        commentService.createComment(comment,id);
    }
}
