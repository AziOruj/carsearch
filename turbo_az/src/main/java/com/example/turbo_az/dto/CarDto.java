package com.example.turbo_az.dto;

import com.example.turbo_az.entity.Comments;
import lombok.Data;

import java.util.Set;

@Data
public class CarDto {

    private Long id;
    private String name;
    private String model;
    private Integer year;
    private Double price;
    private String city;
    private Boolean credit;
    private Boolean barter;
    private Long countComments;
    private Set<Comments> commentsUser;
}
