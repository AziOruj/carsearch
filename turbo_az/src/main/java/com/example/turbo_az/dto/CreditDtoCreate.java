package com.example.turbo_az.dto;

import com.example.turbo_az.entity.Costumer;
import lombok.Data;

@Data
public class CreditDtoCreate {
    private String name;
    private String model;
    private Integer year;
    private Double price;
    private Integer term;
    private Double monthlyPayment;
    private Costumer costumer;
    private Long soldCarId;


}
