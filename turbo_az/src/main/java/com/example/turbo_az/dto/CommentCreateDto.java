package com.example.turbo_az.dto;

import lombok.Data;

@Data
public class CommentCreateDto {
      private String comment;

}
