package com.example.turbo_az.dto;


import lombok.Data;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
public class CreateCarDto {
    @NotNull
    @NotBlank
    @NotEmpty
    private String name;
    @NotNull
    @NotBlank
    @NotEmpty
    private String model;
    @NotNull
    private Integer year;
    @NotNull
    private Double price;
    @NotNull
    @NotBlank
    @NotEmpty
    private String city;
    @NotNull
    private Boolean credit;
    @NotNull
    private Boolean barter;
}
