package com.example.turbo_az.dto;

import com.example.turbo_az.entity.Car;
import com.example.turbo_az.entity.Users;
import lombok.Data;

import java.util.Set;

@Data
public class CommentDto {
    private Long id;
    private String comment;
    private Users userComments;
    private Car carComments;



}
