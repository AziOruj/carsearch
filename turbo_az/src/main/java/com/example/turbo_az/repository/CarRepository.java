package com.example.turbo_az.repository;

import com.example.turbo_az.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CarRepository extends JpaRepository<Car,Long> , JpaSpecificationExecutor<Car> {
}
