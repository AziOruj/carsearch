package com.example.turbo_az.repository;

import com.example.turbo_az.entity.Car;
import com.example.turbo_az.entity.Credit;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CreditRepository extends JpaRepository<Credit,Long> {
}
