package com.example.turbo_az.repository;

import com.example.turbo_az.dto.CommentCreateDto;
import com.example.turbo_az.entity.Comments;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CommentsRepository extends JpaRepository<Comments,Long> {

}
