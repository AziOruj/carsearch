package com.example.turbo_az.repository;

import com.example.turbo_az.entity.Costumer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CostumerRepository extends JpaRepository<Costumer,Long> {
    Boolean existsByFin(String fin);
    Costumer findByFin(String fin);
}
