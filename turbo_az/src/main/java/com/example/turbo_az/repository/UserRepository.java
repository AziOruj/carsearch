package com.example.turbo_az.repository;

import com.example.turbo_az.entity.Users;
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<Users,Long> {
    Boolean existsByLogin(String login);
    Users findByLogin(String login);
}
