package com.example.turbo_az.service;

import com.example.turbo_az.dto.CarDto;
import com.example.turbo_az.dto.CreateCarDto;
import com.example.turbo_az.entity.Car;
import com.example.turbo_az.exception.CarNotFoundException;
import com.example.turbo_az.repository.CarRepository;
import com.example.turbo_az.search.CarSpecialSearch;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.stereotype.Service;

import javax.persistence.criteria.Predicate;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class CarServiceIpl implements CarService {
    private final CarRepository carRepository;
    private final ModelMapper mapper;

    @Override
    public CarDto createCar(CreateCarDto car) {
        return mapper.map(carRepository.
                save(mapper.map(car, Car.class)), CarDto.class);
    }

    @Override
    public CarDto getCarById(Long id) {
        return mapper.map(carRepository.findById(id).
                orElseThrow(CarNotFoundException::new), CarDto.class);
    }

    @Override
    public CarDto updateCar(Long id, CreateCarDto car) {
        if (carRepository.existsById(id)) {
            CarDto carDto = mapper.map(car, CarDto.class);
            carDto.setId(id);
            carRepository.save(mapper.map(carDto, Car.class));
            return carDto;
        } else {
            throw new CarNotFoundException();
        }

    }

    @Override
    public String deleteCar(Long id) {
        if (carRepository.existsById(id)) {
            carRepository.deleteById(id);
            return "Deleting car id: " + id;
        } else {
            throw new CarNotFoundException();
        }


    }

    @Override
    public List<CarDto> getAllCars() {

        return mapper.map(carRepository.findAll(), new TypeToken<List<CarDto>>() {
        }.getType());
    }

    @Override

    public List<CarDto> searchCar(CarSpecialSearch searchCar) {
        List<Car> car = carRepository.findAll(
                (root, query, cb) ->
                {
                    List<Predicate> predicates = new ArrayList<>();
                    if (searchCar.getName() != null &&
                           ! searchCar.getName().isBlank()) {
                        predicates.add(cb.equal(root.get("name"), searchCar.getName()));
                    }
                    if (searchCar.getModel() != null &&
                            ! searchCar.getModel().isBlank()) {
                        predicates.add(cb.equal(root.get("model"), searchCar.getModel()));
                    }
                    if (searchCar.getCity() != null &&
                           ! searchCar.getCity().isBlank()) {
                        predicates.add(cb.equal(root.get("city"), searchCar.getCity()));
                    }
                    if (searchCar.getMinPrice() != null) {
                        predicates.add(cb.greaterThanOrEqualTo(root.get("price"), searchCar.getMinPrice()));
                    }
                    if (searchCar.getMaxPrice() != null) {
                        predicates.add(cb.lessThanOrEqualTo(root.get("price"), searchCar.getMaxPrice()));
                    }
                    if (searchCar.getMinYear() != null) {
                        predicates.add(cb.greaterThanOrEqualTo(root.get("year"), searchCar.getMinYear()));
                    }
                    if (searchCar.getMaxYear() != null) {
                        predicates.add(cb.lessThanOrEqualTo(root.get("year"), searchCar.getMaxYear()));
                    }
                    if (searchCar.getCredit() != null) {
                        predicates.add(cb.equal(root.get("credit"), searchCar.getCredit()));
                    }
                    if (searchCar.getBarter() != null) {
                        predicates.add(cb.equal(root.get("barter"), searchCar.getBarter()));
                    }
                    return cb.and(predicates.toArray(new Predicate[0]));
                });
        return mapper.map(car, new TypeToken<List<CarDto>>() {
        }.getType());

    }

    public List<CarDto> searchCar2(CarSpecialSearch searchCar) {


        if (searchCar.getMinPrice() != null) {
            return new ArrayList<>();
        }
        return null;

        }

}
