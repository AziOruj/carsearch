package com.example.turbo_az.service;

import com.example.turbo_az.dto.CreditDto;
import com.example.turbo_az.dto.CreditDtoCreate;
import com.example.turbo_az.entity.Credit;

public interface CreditService {
    CreditDto createCredit(CreditDtoCreate credit);
}
