package com.example.turbo_az.service;


import com.example.turbo_az.TurboAzApplication;
import com.example.turbo_az.dto.UserCreateDto;
import com.example.turbo_az.entity.Users;
import com.example.turbo_az.exception.UserIsExistsException;
import com.example.turbo_az.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final ModelMapper modelMapper;
    private Boolean isExists(String login){
        return  userRepository.existsByLogin(login);
    }
    private Boolean login(UserCreateDto user){
        return isExists(user.getLogin()) &&
                userRepository.findByLogin(user.getLogin())
                        .getPassword().equals(user.getPassword());
    }

    @Override
    public String createUser(UserCreateDto user) {
        if (isExists(user.getLogin())){
           throw   new UserIsExistsException();
        }
        userRepository.save(modelMapper.map(user, Users.class));
        return "Success registration!!!";
    }

}
