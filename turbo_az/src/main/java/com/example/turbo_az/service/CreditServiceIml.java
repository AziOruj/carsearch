package com.example.turbo_az.service;

import com.example.turbo_az.dto.CreditDto;
import com.example.turbo_az.dto.CreditDtoCreate;
import com.example.turbo_az.entity.Credit;
import com.example.turbo_az.repository.CarRepository;
import com.example.turbo_az.repository.CostumerRepository;
import com.example.turbo_az.repository.CreditRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;


@Service
@Slf4j
@RequiredArgsConstructor

public class CreditServiceIml implements CreditService{
    private final CarRepository carRepository;
    private final CreditRepository creditRepository;
    private final ModelMapper modelMapper;
    private final CostumerRepository costumerRepository;

    @Override
    public CreditDto createCredit(CreditDtoCreate request) {
       Credit credit1= creditRepository.
               save(modelMapper.map(hasCostumer(request),Credit.class));
       carRepository.deleteById(request.getSoldCarId());
       return modelMapper.map(credit1,CreditDto.class);
    }

    public CreditDto hasCostumer(CreditDtoCreate create){
        if (costumerRepository.existsByFin(create.getCostumer().getFin())){
           create.setCostumer(costumerRepository.findByFin(create.getCostumer().getFin()));
       }else{
            costumerRepository.save(create.getCostumer());
        }
       return modelMapper.map(create,CreditDto.class);
    }
}
