package com.example.turbo_az.service;

import com.example.turbo_az.dto.UserCreateDto;
import com.example.turbo_az.dto.UserDto;

public interface UserService {
    String createUser(UserCreateDto user);
}
