package com.example.turbo_az.service;

import com.example.turbo_az.dto.CommentCreateDto;
import com.example.turbo_az.entity.Comments;

public interface CommentService {
    void createComment(CommentCreateDto comment, Long id);
}
