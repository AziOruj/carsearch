package com.example.turbo_az.service;

import com.example.turbo_az.dto.CarDto;
import com.example.turbo_az.dto.CreateCarDto;
import com.example.turbo_az.search.CarSpecialSearch;

import java.util.List;

public interface CarService {
    CarDto createCar(CreateCarDto car) ;
    CarDto getCarById(Long id);
    CarDto updateCar(Long id, CreateCarDto car);
    String deleteCar(Long id);
    List<CarDto> getAllCars();
    List<CarDto> searchCar(CarSpecialSearch search);
}
