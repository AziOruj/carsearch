package com.example.turbo_az.service;

import com.example.turbo_az.TurboAzApplication;
import com.example.turbo_az.dto.CommentCreateDto;
import com.example.turbo_az.dto.CommentDto;
import com.example.turbo_az.entity.Car;
import com.example.turbo_az.entity.Comments;
import com.example.turbo_az.entity.Users;
import com.example.turbo_az.repository.CarRepository;
import com.example.turbo_az.repository.CommentsRepository;
import com.example.turbo_az.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
@Slf4j
public class CommentServiceImpl implements  CommentService{
    private  final CommentsRepository commentsRepository;
    private final CarRepository carRepository;
    private final UserRepository userRepository;
    private  final ModelMapper modelMapper;
    @Override
    public void createComment(CommentCreateDto comment, Long id) {
        String user = TurboAzApplication.activeUser;
        CommentDto commentDto = modelMapper.map(comment,CommentDto.class);
        commentDto.setCarComments(modelMapper.map(carRepository.getById(id), Car.class));
        commentDto.setUserComments(modelMapper.map(userRepository.findByLogin(user), Users.class));
        userRepository.save(commentDto.getUserComments());
        carRepository.save(commentDto.getCarComments());
        commentsRepository.save(modelMapper.map(commentDto,Comments.class));
    }
}
